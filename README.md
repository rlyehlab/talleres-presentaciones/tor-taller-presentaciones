# Taller sobre Tor, presentaciones

Presentaciones para el nivel 1 y 2 del ciclo de talleres sobre Tor en el marco
del proyecto *Enhancing privacy and anonymity in the Latin American context*
por Derechos Digitales y Tor Project.

Desarrollado en [R'lyeh Hacklab](https://rlab.be), CABA, Argentina.

## Colaboradores

Para esta presentación colaboraron:

* HacKan
* Deimidis

## Licencia

![CC BY-SA 4.0](https://licensebuttons.net/l/by-sa/4.0/88x31.png)  
Todo el contenido se encuentra licenciado bajo *[Atribución-CompartirIgual 4.0 Internacional](https://creativecommons.org/licenses/by-sa/4.0/deed.es)* salvo que se explicite lo contrario (algunos contenidos tienen licencia aparte).
